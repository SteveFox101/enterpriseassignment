﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EnterpriseAssignment.DataModel
{
    [MetadataType(typeof(BlogValidation))]
    public partial class Blog
    {
        public virtual IEnumerable<SelectListItem> Categories { get; set; }
    }

    public class BlogValidation
    {
        [Required]
        public String Heading { get; set; }
    }
}