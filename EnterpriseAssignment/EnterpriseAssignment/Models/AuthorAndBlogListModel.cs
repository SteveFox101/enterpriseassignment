﻿using EnterpriseAssignment.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriseAssignment.Models
{
    public class AuthorAndBlogListModel
    {
        public List<Blog> Blogs { get; set; }
        public AspNetUser User { get; set; }
    }
}