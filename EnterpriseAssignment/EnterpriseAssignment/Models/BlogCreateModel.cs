﻿using EnterpriseAssignment.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace EnterpriseAssignment.Models
{
   public class BlogCreateModel
    {
        public System.Guid BlogId { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "The {0} cannot be longer than {1} characters.")]
        public string Heading { get; set; }
        [Required(ErrorMessage = "You must set an {0}")]
        public HttpPostedFileBase Image { get; set; }
        [Required(ErrorMessage = "{0} is required.")]
        public string Content { get; set; }
        public string ImagePath { get; set; }
        public string Username_Fk { get; set; }
        [Required(ErrorMessage = "{0} is required.")]
        public string Tags { get; set; }

        public System.Guid Category_Fk { get; set; }
        public System.DateTime DateCreated { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual Category Category { get; set; }

        public virtual IEnumerable<SelectListItem> Categories { get; set; }
    }
}
