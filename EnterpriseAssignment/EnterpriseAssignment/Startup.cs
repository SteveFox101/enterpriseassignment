﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EnterpriseAssignment.Startup))]
namespace EnterpriseAssignment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
