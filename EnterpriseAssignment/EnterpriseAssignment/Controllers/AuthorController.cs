﻿using EnterpriseAssignment.DataModel;
using EnterpriseAssignment.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace EnterpriseAssignment.Controllers
{
    public class AuthorController : Controller
    {
        Entities Entity = new Entities();
        // GET: Author

        [Authorize]
        [Route("Author/Index/{username}")]
        public ActionResult Index(string username)
        {
            AuthorAndBlogListModel model = new AuthorAndBlogListModel();

            IEnumerable<Blog> enumBlogs = Entity.Blogs.Where(x => x.AspNetUser.UserName == username).OrderByDescending(x => x.DateCreated).AsEnumerable();
            List<Blog> blogs = enumBlogs.ToList();
            ViewData["username"] = username;

            AspNetUser user = Entity.AspNetUsers.Where(x => x.UserName == username).SingleOrDefault();

            model.Blogs = blogs;
            model.User = user;

            return View(model);
        }
        
        [Authorize]
        public ActionResult MyProfile()
        {
            string currentUsername = User.Identity.GetUserName();
            AspNetUser currentUser = Entity.AspNetUsers.Where(x => x.UserName == currentUsername).SingleOrDefault();

            return View(currentUser);
        }
    }
}