﻿using EnterpriseAssignment.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EnterpriseAssignment.Controllers
{
    public class CategoryController : Controller
    {
        Entities Entity = new Entities();
        // GET: Category

        [Authorize]
        [Route("Category/Index/{categoryName}")]
        public ActionResult Index(string categoryName)
        {
            if (categoryName == "MvcDotNet")
                categoryName = "Mvc.Net";

            IEnumerable<Blog> enumBlogs = Entity.Blogs.Where(x => x.Category.CategoryName == categoryName).OrderByDescending(x => x.DateCreated).AsEnumerable();
            List<Blog> blogs = enumBlogs.ToList();
            ViewData["categoryName"] = categoryName;

            return View("Index", "", blogs);
        }
    }
}