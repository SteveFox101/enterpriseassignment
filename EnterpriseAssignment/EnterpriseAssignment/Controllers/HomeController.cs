﻿using EnterpriseAssignment.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EnterpriseAssignment.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        Entities Entity = new Entities();

        public ActionResult Index()
        {
            IEnumerable<Blog> blogs = Entity.Blogs.OrderByDescending(x => x.DateCreated).AsEnumerable();
            List<Blog> blogList = new List<Blog>();

            foreach(Blog b in blogs)
            {
                blogList.Add(b);
            }

            return View(blogList);
        }

        public ActionResult About()
        {
            TempData["Message"] = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            TempData["Message"] = "Your contact page.";

            return View();
        }
    }
}