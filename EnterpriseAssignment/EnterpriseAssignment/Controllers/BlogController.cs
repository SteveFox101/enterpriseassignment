﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dropbox.Api;
using System.Threading.Tasks;
using Dropbox.Api.Users;
using System.IO;
using System.Text;
using Dropbox.Api.Files;
using Dropbox.Api.Sharing;
using EnterpriseAssignment.DataModel;
using EnterpriseAssignment.Models;
using System.Data.Entity.Validation;

namespace EnterpriseAssignment.Controllers
{
    public class BlogController : Controller
    {
        Entities Entity = new Entities();
        // GET: Blog
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Create()
        {
            List<Category> categories = Entity.Categories.ToList();

            BlogCreateModel model = new BlogCreateModel();
            model.Categories = GetSelectListItems(categories);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Create(BlogCreateModel blog, HttpPostedFileBase Image)
        {
            List<Category> categories = Entity.Categories.ToList();
            blog.Categories = GetSelectListItems(categories);

            try
            {
                if (ModelState.IsValid)
                {
                    if(Image.ContentLength <= 150000000)
                    {
                        DropboxClient dbx = new DropboxClient("9Svq7UQdvCAAAAAAAAAAH7YMzry4Hg8aOuIJ5agh81BxXaJ-Szn_BvpEYz18UhIc");

                        MemoryStream target = new MemoryStream
                        {
                            Position = 0
                        };
                        Image.InputStream.CopyTo(target); // generates problem in this line
                        target.Position = 0;

                        string currentUser = User.Identity.Name;

                        string imageId = Guid.NewGuid().ToString();

                        FileMetadata updated = await dbx.Files.UploadAsync("/" + currentUser + "/" + imageId,
                        WriteMode.Overwrite.Instance,
                        body: target);

                        SharedLinkMetadata sharedLink = await dbx.Sharing.CreateSharedLinkWithSettingsAsync("/" + currentUser + "/" + imageId);

                        Blog b = new Blog() { BlogId = Guid.NewGuid(), Heading = blog.Heading, Content = blog.Content, Category_Fk = blog.Category_Fk, Username_Fk = User.Identity.GetUserId(), DateCreated = DateTime.Now, Image = sharedLink.Url.Replace("dl=0", "raw=1"), Tags = blog.Tags, Categories = GetSelectListItems(categories) }; 

                        Entity.Blogs.Add(b);
                        Entity.SaveChanges();

                        TempData["Message"] = "Successfully created!";

                        return View(blog);
                    }

                    TempData["Error"] = "File size must be smaller than 150MB!";
                    return View(blog);
                }

                else
                {
                    TempData["Error"] = "Please fill in all details.";
                    return View(blog);
                }
            }

            catch(DbEntityValidationException ex)
            {
                TempData["Error"] = "Something went wrong. We are working to fix this.";
                return View(blog);
            }
        }
        
        [Authorize]
        public ActionResult Edit(Guid id)
        {
            try
            {
                Blog blogDetails = Entity.Blogs.Where(x => x.BlogId == id).SingleOrDefault();

                if(blogDetails.AspNetUser.UserName == User.Identity.Name)
                    return View(blogDetails);

                TempData["Error"] = "This blog does not belong to you!";
                return View();
            }

            catch
            {
                return View();
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(Blog model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.Username_Fk == User.Identity.GetUserId())
                    {
                        Blog oldDetails = Entity.Blogs.Where(x => x.BlogId == model.BlogId).SingleOrDefault();

                        oldDetails.Heading = model.Heading;
                        oldDetails.Content = model.Content;
                        oldDetails.Tags = model.Tags;

                        Entity.SaveChanges();

                        TempData["Message"] = "Successfully edited!";

                        return RedirectToAction("Index", "Home");
                    }

                    TempData["Error"] = "This blog does not belong to you!";
                    return RedirectToAction("Index", "Home");
                }

                TempData["Error"] = "Please fill all fields!";
                return View();
            }

            catch
            {
                TempData["Error"] = "Something went wrong with the edit. Make sure you insert the correct information";
                return View(model);
            }
        }

        [Authorize]
        public ActionResult Delete(Guid id)
        {
            try
            {
                Blog toDelete = Entity.Blogs.Where(x => x.BlogId == id).SingleOrDefault();
                if (User.Identity.Name == toDelete.AspNetUser.UserName)
                {

                    Entity.Blogs.Remove(toDelete);
                    Entity.SaveChanges();

                    TempData["Message"] = "Successfully deleted!";

                    return RedirectToAction("Index", "Home");
                }

                TempData["Error"] = "Something went wrong. Try again later.";
                return RedirectToAction("Index", "Home");
            }

            catch
            {
                TempData["Error"] = "Something went wrong with the delete. This Blog might not exist anymore!";
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize]
        public ActionResult BlogDetails(string id)
        {
            try
            {
                Guid blogId = new Guid(id);
                Blog blog = Entity.Blogs.Where(x => x.BlogId == blogId).SingleOrDefault();

                return View(blog);
            }

            catch
            {
                TempData["Error"] = "Blog-id format was incorrect.";
                return View();
            }
        }

        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<Category> elements)
        {
            var selectList = new List<SelectListItem>();
            
            foreach (var element in elements)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element.CategoryId.ToString(),
                    Text = element.CategoryName
                });
            }

            return selectList;
        }
    }
}